from flask import Flask
import requests
import random
from flask import render_template
app = Flask(__name__)

@app.errorhandler(404) 
def not_found(e): 
    return render_template("not_found.html")
    
    
# Page d'accueil listant l'ensemble des dinosaures
@app.route('/')
def index():
    response=requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    dinos=response.json()
    return render_template('accueil.html',dinos=dinos)
    
    
# Page de détails sur un dinosaure
@app.route('/dinosaur/')
@app.route('/dinosaur/<slug>')
def dino(slug):
    response = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    dinos = response.json()
    for dino in dinos:
        if (dino.get("slug")) == slug:
            r=dino.get("uri")
            responsed=requests.get(str(r))
            dino_slug=responsed.json()
            dino_random = random.sample(dinos,3)
            return render_template('liste.html', dino_slug=dino_slug,dino_random=dino_random)
  



        
         
